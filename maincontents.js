﻿conf_count = 0;
        var jokes = new Array( );
        jokes[1]="Why do I get so angry? Because people keep pushing my buttons!";
        jokes[2]="What is my favorite type of music? Heavy metal";
        jokes[3]="Why was I feeling bad? I had a virus";
        jokes[4]="Why did I go to the mall? Because I had hardware and software but needed underwear";
        jokes[5]="Why was I so tired when I got here? I had a hard drive";
        jokes[6]="Why did I cross the road? I was programmed by a chicken";
        jokes[7]="Why can’t bad programmers drive well? They can’t C sharp";
        jokes[8]="Want to know my favorite drink? It’s a screwdriver";
        jokes[9]="Want to know my favorite dance? It’s the robot";
        jokes[10]="What do you call a guy that builds polite infrastructure projects? A civil Engineer.";

        jokes[11]="Why did the road cross the road? Because civil engineers & urban planners don't like roundabouts!";

        jokes[12]="Even if I end up being a civil engineer I won't build tunnels. Because it's boring.";

        jokes[13]="Everyone hated the egotistical civil engineer. He got too big for his bridges.";

        var speak = new Array( );
        speak[0] = "";
        speak[1] = "Take a picture with me.";
        speak[2] = "Pick a song and let's boogie.";
        speak[3] = "Become a robot.";
        speak[4] = "see our pictures from today on our slack channel";
        speak[5] = "";

//window.external.ChangeLanguage("en-us");
function FC_ContentsCall(strContentsName, strLanguage)
{
   // alert(strContentsName);
    
    switch (strContentsName)
    {
        case "Welcome":
            PlaySpeech("Hi, I’m Tracey, welcome to the Novartis booth. Please click around on my screen and then speak to my smart human friends in the booth.");
            break;
        case "Home":
            //writeCookie("NavigationState", true, 30);
           location.href = "../../maincontents.htm";
           break;

        case "MatchGame":
            //writeCookie("NavigationState", true, 30);
           location.href = "Contents/MatchGame/index.html";
           break;

        case "Selfie":
            location.href = "Contents/Selfie/index.html";
            PlaySpeech(speak[1]);
            break;

        case "Dance":
            location.href = "Contents/Dance/index.html";
            PlaySpeech(speak[2]);
            break;

            case "Slides":
            location.href = "Contents/Slides/index.html";
           /* PlaySpeech(speak[2]); */
            break;

        case "Avatar":
            location.href = "Contents/RobotAvatar/index.htm";
            PlaySpeech(speak[3]);
            break;

        case "Owner":
            location.href = "Contents/Owner/index.html";
            //PlaySpeech(speak[4]);
            break;

        case "Mentor":
            location.href = "Contents/Mentor/index.html";
            //PlaySpeech(speak[5]);
            break;

        case "Rising":
            location.href = "Contents/Rising/index.html";
            //PlaySpeech(speak[5]);
            break;

        case "Rental":
            location.href = "Contents/Rental/index.html";
            //PlaySpeech(speak[5]);
            break;

        case "RentalSlide":
            location.href = "../RentalSlide/index.html";
            //PlaySpeech(speak[5]);
            break;

        case "RentalVideo":
            location.href = "../RentalVideo/index.html";
            //PlaySpeech(speak[5]);
            break;

        case "BackToRental":
            location.href = "../Rental/index.html";
            //PlaySpeech(speak[5]);
            break;
        
        
        
            
        case "Config":
            if(conf_count === 3)
            {
                conf_count = 0;
                location.href = "Contents/Config/Config.htm";
            }
            else
            {
                conf_count++;
                console.log(conf_count);
            }

            break;
        default:
            break;
    } // end switch(strContentsName)
} // end FC_ContentsCall



function OnUserApproached()
{
    //PlaySpeech("Hello, welcome to the BWl Airport. Please press a button on my screen to begin.");
}


function ShowPopup(){

// get the screen height and width
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    // calculate the values for center alignment
    var dialogTop =  '30%';//(maskHeight/3) - ($('#dialog-box').height());
    var dialogLeft = (maskWidth/2) - ($('#dialog-box').width()/2);
    // assign values to the overlay and dialog box
    $('#dialog-overlay').css({height:maskHeight, width:maskWidth}).show();
    $('#dialog-box').css({top:dialogTop, left:dialogLeft}).show();
    document.getElementById('dialog-box').innerHTML = '<a href="#" class="button">Close</a><div class="dialog-content"><div id="dialog-message"><img width="800" src="assets/contact.png"/></div></div>';
}
function ShowPopupForm(){

// get the screen height and width
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    // calculate the values for center alignment
    var dialogTop =  '30%';//(maskHeight/3) - ($('#dialog-box').height());
    var dialogLeft = (maskWidth/2) - ($('#dialog-box').width()/2);
    // assign values to the overlay and dialog box
    $('#dialog-overlay').css({height:maskHeight, width:maskWidth}).show();
    $('#dialog-box').css({top:dialogTop, left:dialogLeft}).show();
    document.getElementById('dialog-box').style.display = "block";
    //document.getElementById('dialog-box').innerHTML = '<a href="#" class="button">Close</a><div class="dialog-content"><div id="dialog-message"><img width="800" src="assets/contact.png"/></div></div>';
}



$(document).ready(function(){

    //$('.navigation').hide();
    /*$('.get-started').click(function(){
        $('.welcome-text, .get-started, footer').hide();
        $('.navigation').show();
        
    });
    if (readCookie("NavigationState")) {
        $('.welcome-text, .get-started, footer').hide();
        $('.navigation').show();
    }
    $('.close-menu').click(function(){
        $('.navigation').hide();
        $('.welcome-text, .get-started, footer').show();
        writeCookie("NavigationState", false, 30);
    });*/
    

    $('a.close').click(function () {
        $('#dialog-overlay, #dialog-box').hide();
        return false;
    });

  


});

function OnJoystickControlled(strPara){
    var btn_info = strPara.split(',')[4];


    if(btn_info[0] == '1'){
           // window.external.ChangeLanguage("en-us");
            SetVolume(1);
            window.external.PlaySpeech("Brook! Brook! Brook! It’s time to start the program. My battery is down to 12%.");     
    }

    if(btn_info[1] == '1'){
        //window.external.ChangeLanguage("en-us");
        SetVolume(1);
        window.external.PlaySpeech("I'm here to keep you from getting distracted.");
        
    }
    if(btn_info[2] == '1'){
        //window.external.ChangeLanguage("en-us");
            SetVolume(1);
            window.external.PlaySpeech("I would like to ask that after each of tonight's winners receives their award, to quickly proceed back down to your table.");
    }
    if(btn_info[3] == '1'){
        //window.external.ChangeLanguage("en-us");
        SetVolume(1);
       window.external.PlaySpeech("I am not programmed to relax. I am programmed to keep the program moving.");
        
    }
    if(btn_info[4] == '1'){
        SetVolume(1);
        window.external.PlaySpeech("The professional photographer will be set up outside the ballroom after the event so that you can also get a great photo of your team and your award. I will make sure that we conclude our Gala at a reasonable hour.");
        //FC_ContentsCall('Config');       
    }
    if(btn_info[5] == '1'){
        SetVolume(1);
        window.external.PlaySpeech("Welcome to the CMAA Gala");
        //FC_ContentsCall('Config');       
    }
    if(btn_info[6] == '1'){
        SetVolume(1);
        window.external.PlaySpeech("You look nice, I like you");
        //FC_ContentsCall('Config');       
    }
}